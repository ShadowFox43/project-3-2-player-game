using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCMove : MonoBehaviour
{
    [SerializeField]
    Transform destination;

    NavMeshAgent aiNavMeshAgent;

    // Start is called before the first frame update
    void Start()
    {
        aiNavMeshAgent = this.GetComponent<NavMeshAgent>();

        if (aiNavMeshAgent == null)
        {
            Debug.LogError("The Nav Mesh Agent is not attached to " + gameObject.name);
        }
        else
        {
            SetDaDestination();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetDaDestination()
    {
        if (destination != null)
        {
            Vector3 targetVector = destination.transform.position;
            aiNavMeshAgent.SetDestination(targetVector);
        }
    }
}
