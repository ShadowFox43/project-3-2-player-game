using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickOn : MonoBehaviour
{
    [SerializeField]
    private Material red;
    [SerializeField]
    private Material green;

    private MeshRenderer myRend;
    public GameObject playerControlledWall;
    public Vector3 raisedPosition;
    public Vector3 raisedWallScale;
    public Vector3 loweredPosition;
    public Vector3 loweredWallScale;
    public bool wallRaised;

    // Start is called before the first frame update
    void Start()
    {
        myRend = GetComponent<MeshRenderer>();
    }

    public void ClickedOnMe()
    {
        if(wallRaised == false)
        {
            myRend.material = red;
            playerControlledWall.transform.position = new Vector3(raisedPosition.x, raisedPosition.y, raisedPosition.z);
            playerControlledWall.transform.localScale = new Vector3(raisedWallScale.x, raisedWallScale.y, raisedWallScale.z);
            wallRaised = true;
        }
        else
        {
            myRend.material = green;
            playerControlledWall.transform.position = new Vector3(loweredPosition.x, loweredPosition.y, loweredPosition.z);
            playerControlledWall.transform.localScale = new Vector3(loweredWallScale.x, loweredWallScale.y, loweredWallScale.z);
            wallRaised = false;
        }
    }
}
